﻿using System;
using System.Collections.Generic;
using System.Linq;
using CheckGeneratorApp.Interfaces;

namespace CheckGeneratorApp.Implementations
{
    public class Tape : ITape
    {
        public IList<ICheck> Checks { get; set; }
        public int Count => Checks.Count;
        /// <summary>
        /// Сумма по ленте
        /// </summary>
        /// <param name="rounder">Правильно округления</param>
        /// <returns>Сумма</returns>
        public decimal S(IRounder rounder) => rounder.Round(Checks.Sum(check => check.S(rounder)));
        /// <summary>
        /// Сумма по ленте за дату
        /// </summary>
        /// <param name="rounder">Правило округления</param>
        /// <param name="date">Дата</param>
        /// <returns>Сумма</returns>
        public decimal S(IRounder rounder, DateTime date) => rounder.Round(Checks.Where(check => check.CheckDate == date).Sum(check => check.S(rounder)));

        public Tape()
        {
            Checks = new List<ICheck>();
        }

        public void AddCheck(ICheck check)
        {
            Checks.Add(check);
        }

        /// <summary>
        /// Список дат в ленте
        /// </summary>
        /// <returns>Список дат</returns>
        public List<DateTime> TapeDates()
        {
            var dates = new List<DateTime>();
            foreach (var check in Checks)
            {
                if (dates.FindAll(x => x == check.CheckDate).Count == 0)
                {
                    dates.Add(check.CheckDate);
                }
            }

            return dates;
        }
    }
}