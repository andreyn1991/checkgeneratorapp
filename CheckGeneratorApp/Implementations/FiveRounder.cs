﻿using System;
using CheckGeneratorApp.Interfaces;

namespace CheckGeneratorApp.Implementations
{
    public class FiveRounder : IRounder
    {
        public decimal Round(decimal value)
        {
            var result = Math.Round(value, 2) * 100;
            if (result % 10 < 5)
            {
                result = Math.Floor(result / 10) / 10;
            }
            else if (result % 10 > 5)
            {
                result = Math.Ceiling(result / 10) / 10;
            }
            else
            {
                result = value;
            }

            return result;
        }
    }
}