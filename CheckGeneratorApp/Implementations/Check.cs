﻿using System;
using System.Collections.Generic;
using CheckGeneratorApp.Interfaces;

namespace CheckGeneratorApp.Implementations
{
    public class Check : ICheck
    {
        public Check()
        {
            Rows = new List<IRow>();
        }

        public int Id { get; set; }
        public DateTime CheckDate { get; set; }
        public List<IRow> Rows { get; }

        public void AddRow(IRow row)
        {
            Rows.Add(row);
        }

        /// <summary>
        /// Сумма чека
        /// </summary>
        /// <param name="rounder">Правило округления</param>
        /// <returns>Сумма</returns>
        public decimal S(IRounder rounder)
        {
            decimal s = 0;
            foreach (var row in Rows)
            {
                s += row.S;
            }

            return rounder.Round(s);
        }
    }
}