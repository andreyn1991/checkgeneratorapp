﻿using CheckGeneratorApp.Interfaces;

namespace CheckGeneratorApp.Implementations
{
    public class Row : IRow
    {
        public Row(int id, int count, decimal price)
        {
            Id = id;
            Count = count;
            Price = price;
        }
        public int Id { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
        public decimal S => new MathRounder().Round(Count * Price);
    }
}