﻿using System;
using CheckGeneratorApp.Interfaces;

namespace CheckGeneratorApp.Implementations
{
    public class MathRounder : IRounder
    {
        public decimal Round(decimal value)
        {
            return Math.Round(value, 2);
        }
    }
}