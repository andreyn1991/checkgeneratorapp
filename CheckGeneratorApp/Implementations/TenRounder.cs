﻿using System;
using CheckGeneratorApp.Interfaces;

namespace CheckGeneratorApp.Implementations
{
    public class TenRounder : IRounder
    {
        public decimal Round(decimal value)
        {
            return Math.Round(value, 1);
        }
    }
}