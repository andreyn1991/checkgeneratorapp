﻿namespace CheckGeneratorApp.Interfaces
{
    public interface IRounder
    {
        decimal Round(decimal value);
    }
}