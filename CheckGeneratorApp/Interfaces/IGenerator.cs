﻿namespace CheckGeneratorApp.Interfaces
{
    public interface IGenerator
    {
        ITape GTape { get; }
        void GenerateTape();
    }
}