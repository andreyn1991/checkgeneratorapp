﻿namespace CheckGeneratorApp.Interfaces
{
    public interface IRow
    {
        int Id { get; set; }
        int Count { get; set; }
        decimal Price { get; set; }
        decimal S { get; }
    }
}