﻿using System;
using System.Collections.Generic;

namespace CheckGeneratorApp.Interfaces
{
    public interface ICheck
    {
        int Id { get; set; }
        DateTime CheckDate { get; set; }
        List<IRow> Rows { get; }
        void AddRow(IRow row);
        decimal S(IRounder rounder);
    }
}