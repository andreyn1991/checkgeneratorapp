﻿using System;
using System.Collections.Generic;

namespace CheckGeneratorApp.Interfaces
{
    public interface ITape
    {
        IList<ICheck> Checks { get; set; }
        int Count { get; }
        decimal S(IRounder rounder);
        decimal S(IRounder rounder, DateTime date);
        void AddCheck(ICheck check);
        List<DateTime> TapeDates();
    }
}