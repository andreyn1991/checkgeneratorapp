﻿namespace CheckGeneratorApp.Interfaces
{
    public interface IDataManager
    {
        void SaveData(ITape tape, string path);
        ITape LoadData(string path);
    }
}