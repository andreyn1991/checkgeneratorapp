﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using CheckGeneratorApp.Generators;
using CheckGeneratorApp.Data;
using Autofac;
using CheckGeneratorApp.Interfaces;
using CheckGeneratorApp.Implementations;

namespace CheckGeneratorApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static IContainer IoC;
        private readonly IRounder _mathRounder;
        private readonly IRounder _fiveRounder;
        private readonly IRounder _tenRounder;

        public MainWindow()
        {
            InitializeComponent();
            var builder = new ContainerBuilder();
            builder.RegisterType<Check>().As<ICheck>();
            builder.RegisterType<Row>().As<IRow>();
            builder.RegisterType<Tape>().As<ITape>();
            builder.RegisterType<JsonDataManager>().As<IDataManager>();
            builder.RegisterType<TapeGenerator>().As<IGenerator>();
            builder.RegisterType<List<Check>>().As<IList>();
            IoC = builder.Build();

            _mathRounder = new MathRounder();
            _fiveRounder = new FiveRounder();
            _tenRounder = new TenRounder();
        }

        private void BGenerateTape_Click(object sender, RoutedEventArgs e)
        {
            using (var scope = IoC.BeginLifetimeScope())
            {
                var generator = scope.Resolve<IGenerator>();
                generator.GenerateTape();
                scope.Resolve<IDataManager>().SaveData(generator.GTape, "source.json");
            }
        }

        private void BLoadTape_Click(object sender, RoutedEventArgs e)
        {
            using (var scope = IoC.BeginLifetimeScope())
            {
                var tape = scope.Resolve<IDataManager>().LoadData("source.json");
                LTapeBrowser.Items.Clear();
                if (tape == null)
                {
                    return;
                }
                LTapeBrowser.Items.Add("Сумма ленты итого (Математическое округление) = " + tape.S(_mathRounder));
                LTapeBrowser.Items.Add("Сумма ленты итого (Округление до 5 копеек) = " + tape.S(_fiveRounder));
                LTapeBrowser.Items.Add("Сумма ленты итого (Округление до 10 копеек) = " + tape.S(_tenRounder));
                foreach (var date in tape.TapeDates().OrderBy(d => d.Date))
                {
                    LTapeBrowser.Items.Add("Сумма ленты на " + date.ToShortDateString() + " (Математическое округление) = " + tape.S(_mathRounder, date));
                    LTapeBrowser.Items.Add("Сумма ленты на " + date.ToShortDateString() + " (Округление до 5 копеек) = " + tape.S(_fiveRounder, date));
                    LTapeBrowser.Items.Add("Сумма ленты на " + date.ToShortDateString() + " (Округление до 10 копеек) = " + tape.S(_tenRounder, date));
                }
            }
        }
    }
}
