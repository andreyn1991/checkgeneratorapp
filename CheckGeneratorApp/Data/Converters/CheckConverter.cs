﻿using System;
using CheckGeneratorApp.Implementations;
using CheckGeneratorApp.Interfaces;
using Newtonsoft.Json;

namespace CheckGeneratorApp.Data.Converters
{
    public class CheckConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(ICheck));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return serializer.Deserialize(reader, typeof(Check));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value, typeof(Check));
        }
    }
}