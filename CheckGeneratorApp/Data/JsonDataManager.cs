﻿using Newtonsoft.Json;
using System.IO;
using CheckGeneratorApp.Interfaces;
using System.Windows;
using CheckGeneratorApp.Data.Converters;

namespace CheckGeneratorApp.Data
{
    public class JsonDataManager : IDataManager
    {
        public void SaveData(ITape tape, string path)
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(tape));
        }

        public ITape LoadData(string path)
        {
            if (File.Exists(path))
            {
                var settings = new JsonSerializerSettings { Formatting = Formatting.Indented };
                settings.Converters.Add(new TapeConverter());
                settings.Converters.Add(new CheckConverter());
                settings.Converters.Add(new RowConverter());
                return JsonConvert.DeserializeObject<ITape>(File.ReadAllText(path), settings);
            }
            else
            {
                MessageBox.Show("Неверно указан путь к файлу");
                return null;
            }
        }
    }
}