﻿using System;
using CheckGeneratorApp.Interfaces;
using CheckGeneratorApp.Implementations;

namespace CheckGeneratorApp.Generators
{
    public class TapeGenerator : IGenerator
    {
        public ITape GTape { get; }
        private static Random _random;

        public TapeGenerator()
        {
            GTape = new Tape();
            _random = new Random();
        }

        public void GenerateTape()
        {
            for (var i = 0; i < 100; i++)
            {
                GTape.AddCheck(GetNewCheck(i));
            }
        }

        private static ICheck GetNewCheck(int id)
        {
            ICheck check = new Check
            {
                Id = id,
                CheckDate = GetRandomDate()
            };
            for (var i = 0; i < _random.Next(11); i++)
            {
                check.AddRow(GetNewRow(i));
            }
            return check;
        }

        private static DateTime GetRandomDate()
        {
            return DateTime.Now.Date.AddDays(_random.Next(-3, 4));
        }

        private static IRow GetNewRow(int id)
        {
            decimal price = _random.Next(5, 10000);
            price /= 100;
            IRow row = new Row(id, _random.Next(21), price);
            return row;
        }
    }
}